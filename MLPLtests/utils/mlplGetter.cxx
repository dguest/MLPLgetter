/// @file mlplGetter.cxx
/// @brief Reading the DIPZ labels of an event and output the calculate its MLPL discriminant variable
/// The DIPZ labels are: HLT_AntiKt4EMTopoJets_subjesIS_fastftagAuxDyn.dipz20230223_z & HLT_AntiKt4EMTopoJets_subjesIS_fastftagAuxDyn.dipz20230223_negLogSigma2

// System include(s):
#include <memory>
#include <vector>

// ROOT include(s):
#include <TFile.h>
#include <TError.h>

// AnalysisBase tool include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/ReturnCheck.h"

// EDM include(s):
#include "AthContainersInterfaces/AuxTypes.h"
#include "xAODCore/tools/PerfStats.h"
#include "xAODCore/tools/IOStats.h"
#include "xAODCore/tools/ReadStats.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/Jet.h"
#include "xAODCore/ShallowAuxContainer.h"
#include "xAODCore/ShallowCopy.h"


int main( int argc, char* argv[] ) {

   // The name of the application:
   static const char* APP_NAME = "MlplGetter";

   // Check that at least one input file was provided:
   if( argc < 2 ) {
      Error( APP_NAME, "Usage: %s <file1> [file2] ...", APP_NAME );
      return 1;
   }

   // Set up the environment:
    xAOD::Init().ignore();

   // Set up the event object:
   xAOD::TEvent event( xAOD::TEvent::kClassAccess );

   // Start the measurement:
   auto& ps = xAOD::PerfStats::instance();
   ps.start();

   // Dummy variable:
   long long int dummy = 0;

   // Loop over the specified files:
   for( int i = 1; i < argc; ++i ) {

      // Open the file:
      std::unique_ptr< TFile > ifile( TFile::Open( argv[ i ], "READ" ) );
      if( ( ! ifile.get() ) || ifile->IsZombie() ) {
         Error( APP_NAME, "Couldn't open file: %s", argv[ i ] );
         return 1;
      }
      Info( APP_NAME, "Opened file: %s", argv[ i ] );

      // Connect the event object to it:
      event.readFrom( ifile.get() ).ignore();

      // Loop over its events:
      const Long64_t entries = event.getEntries();
      for( Long64_t entry = 0; entry < entries; ++entry ) {

         // Load the event:
         if( event.getEntry( entry ) < 0 ) {
            Error( APP_NAME, "Couldn't load entry %lld from file: %s",
                   entry, argv[ i ] );
            return 1;
         }

         // Print some status:
         if( ! ( entry % 500 ) ) {
            Info( APP_NAME, "Processing entry %lld / %lld", entry, entries );
         }

         // Read the Jet Container
         const xAOD::JetContainer* jets = 0;
         event.retrieve( jets, "HLT_AntiKt4EMTopoJets_subjesIS_fastftagAuxDyn" ).ignore();
         
         // Loop over the jets in the event
         for(const xAOD::Jet* jet : * jets ){
         std::cout << "Let's go!" << std::endl;
         }
         
         break;

      }
   }

   // Make a dummy printout just to make sure that C++ optimisations don't
   // remove the file reading commands:
   Info( APP_NAME, "dummy = %lld", dummy );

   // Stop the measurement:
   ps.stop();
   xAOD::IOStats::instance().stats().Print( "Summary" );

   // Return gracefully:
   return 0;
}
