AthAnalysis xAOD MLPL Getter
=============================

This is some code to get the mlpl of an event inside an xAOD using the xAOD EDM, using the
AthAnalysis release.

To compile the code, do something like:

```bash
git clone https://gitlab.cern.ch/maboelel/MLPLgetter.git
asetup AthAnalysis,24.2.14
mkdir build
cd build/
cmake ../MLPLgetter/
make
```
